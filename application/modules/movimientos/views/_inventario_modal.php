<div id="inventarioModal" class="modal fade in" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Inventario</h4>
      </div>
      <div class="modal-body">
        <?php 
          $this->load->library('grocery_crud')
                     ->library('ajax_grocery_crud');
          $crud = new ajax_grocery_crud();
          $crud->set_table('productosucursal')
               ->set_subject('Inventario')
               ->set_theme('bootstrap2')
               ->unset_add()
               ->unset_edit()
               ->unset_delete()
               ->unset_print()
               ->unset_export()
               ->unset_read()
               ->unset_jquery()
               ->unset_jquery_ui()
               ->set_primary_key('id')                              
               ->display_as('sucursales_id','Sucursal')
               ->columns('Codigo','nombre_comercial','nombre_generico','stock','precio_venta','sucursal_id')
               ->set_url('movimientos/productos/inventario_modal/');
          $crud = $crud->render(1);
          echo $crud->output;
          ?>

          <script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>
          <script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>
          <script src="<?= base_url() ?>assets/grocery_crud/js/common/list.js"></script>          
          <script src="<?= base_url() ?>assets/grocery_crud/themes/bootstrap2/js/cookies.js"></script>
          <script src="<?= base_url() ?>assets/grocery_crud/themes/bootstrap2/js/flexigrid.js"></script>
          <script src="<?= base_url() ?>assets/grocery_crud/themes/bootstrap2/js/jquery.form.js"></script>
          <script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js"></script>
          <script src="<?= base_url() ?>assets/grocery_crud/themes/bootstrap2/js/jquery.printElement.min.js"></script>
          <script src="<?= base_url() ?>assets/grocery_crud/themes/bootstrap2/js/pagination.js"></script>
          <script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js"></script>
          <script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js"></script>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<script>
  $("#inventarioModal").on("shown.bs.modal",function(){
    $("#inventarioModal .searchRow").removeClass('hide');
    setTimeout(function(){$($("#inventarioModal input[name='search_text[]']")[1]).focus();},600);
  });
</script>