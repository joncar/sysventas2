<form action="" method="post" onsubmit="insertarCategoria(this); return false;">
	<div class="modal fade" id="categorias" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-lg" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">Añadir categoria</h4>
	            </div>
	            <div class="modal-body">                
	            	<div class="form-group" id="denominacion_field_box">
                    	<label for="field-denominacion" id="denominacion_display_as_box" style="width:100%">
                                Denominacion :
                        </label>
                        <input id="field-denominacion" name="denominacion" class="form-control denominacion" type="text" value="" maxlength="35">
                	</div>
                	<div class="form-group" id="control_vencimiento_field_box">
                    	<label for="field-control_vencimiento" id="control_vencimiento_display_as_box" style="width:100%">
                                Control vencimiento<span class="required">*</span>  :
                        </label>
                        <label>
                        <input id="field-control_vencimiento-true" name="control_vencimiento" class="ace" type="radio" value="1"><span class="lbl">Si</span></label>  <label><input id="field-control_vencimiento-true" name="control_vencimiento" class="ace" type="radio" value="0"><span class="lbl">No</span></label>
            		</div>
	           		<div id="report-error"></div>
	            </div>
	            <div class="modal-footer">
	            	<button type="submit" class="btn btn-success">Guardar</button>        
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>        
	            </div>
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</form>

<script>
	function insertarCategoria(obj){
		$('#categorias #report-error').removeClass('alert alert-success alert-danger').html('');
		insertar('movimientos/productos/categorias/insert',obj,'',function(data){
			if(data.success){
				$('#categorias').modal('toggle');
				$.post('<?= base_url() ?>movimientos/productos/categorias/json_list',{per_page:1000},function(medidas){
					var opt = '';
					medidas = JSON.parse(medidas);
					for(var i in medidas){
						var selected = data.insert_primary_key==medidas[i].id?'selected="true"':'';
						opt+= '<option value="'+medidas[i].id+'" '+selected+'>'+medidas[i].denominacion+'</option>';
					}
					$("select[name='categoria_id']").html(opt);
					$("select[name='categoria_id']").chosen().trigger('liszt:updated');
				});
			}else{
				$('#categorias #report-error').addClass('alert alert-danger').html(data.error_message);
			}
		},function(data){$('#categorias #report-error').addClass('alert alert-danger').html(data.error_message);});
	}
</script>