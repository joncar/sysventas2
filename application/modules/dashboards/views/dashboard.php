<?php
	$resumen = $this->db->query("
			SELECT 

				format(sum(efectivo.Caja_inicial),0,'de_DE') as Caja_inicial, 
				format(sum(efectivo.Ingreso),0,'de_DE') as Total_ingreso, 
				format(sum(efectivo.Egreso),0,'de_DE') as Total_egreso, 
				format((sum(efectivo.Ingreso)+sum(efectivo.Caja_inicial))-sum(efectivo.Egreso),0,'de_DE') as Efectivo_a_rendir 
				FROM(
				SELECT 
				consulta.Movimiento, 

				if(consulta.Tipo = 'Caja_inicial',consulta.total,0) as Caja_inicial, 
				if(consulta.Tipo = 'Ingreso',consulta.total,0) as Ingreso, 
				if(consulta.Tipo = 'Egreso',consulta.total,0) as Egreso 
				FROM( 

				SELECT 

				'Caja_inicial' as Tipo, 

				'Monto_inicial' as Movimiento, 
				cajadiaria.monto_inicial as total 
				FROM 
				cajadiaria WHERE cajadiaria.id = ".$this->user->cajadiaria." 

				UNION ALL 
				SELECT 
				'Ingreso' as Tipo, 
				'Venta_contado' as Movimiento, 
				ifnull(sum(ventadetalle.totalcondesc),0) as total 
				FROM ventas INNER JOIN ventadetalle on ventadetalle.venta = ventas.id WHERE ventas.transaccion = 1 and ventas.status = 0 and ventas.cajadiaria = ".$this->user->cajadiaria." 
				UNION ALL 
				SELECT 
				'Ingreso' as Tipo, 
				'Entrega_credito' as Movimiento, 
				ifnull(sum(cr.entrega_inicial),0) as total 
				FROM creditos cr 
				INNER JOIN ventas on ventas.id = cr.ventas_id WHERE (cr.anulado = 0 or cr.anulado is null) and ventas.status != -1 and ventas.cajadiaria = ".$this->user->cajadiaria." 
				UNION ALL 
				SELECT 
				'Ingreso' as Tipo,
				'Pago_credito' as Movimiento, 
				ifnull(sum(total_pagado),0) as total 
				FROM pagocliente WHERE (anulado = 0 or anulado is null) and cajadiaria = ".$this->user->cajadiaria." 
				UNION ALL 
				SELECT 
				'Egreso' as Tipo, 
				'Gastos_varios' as Movimiento, 
				IFNULL(sum(gastos.monto),0) as total 
				FROM gastos WHERE 1 and gastos.cajadiaria = ".$this->user->cajadiaria.") AS consulta) AS efectivo;");
?>
<div class="row" style="margin:30px 0;">
	<div class="col-xs-12" style="text-align: center">
		<div class="infobox-container" style="display: inline-block;">
			<div class="infobox infobox-green infobox-small infobox-dark col-xs-12 col-sm-4">
		          <div class="infobox-chart">
		            <span class="sparkline" data-values="3,4,2,3,4,4,2,2">
		              <i class="fa fa-money fa-2x" style="vertical-align: top"></i>
		            </span>
		          </div>

		          <div class="infobox-data">
		            <div class="infobox-content">Ingresos</div>
		            <div class="infobox-content"><?= $resumen->row()->Total_ingreso ?> Gs.</div>
		          </div>
		    </div>        
		    <div class="infobox infobox-blue infobox-small infobox-dark col-xs-12 col-sm-4">
		          <div class="infobox-chart">
		            <span class="sparkline" data-values="3,4,2,3,4,4,2,2">
		              <i class="fa fa-money fa-2x" style="vertical-align: top"></i>
		            </span>
		          </div>

		          <div class="infobox-data">
		            <div class="infobox-content">Egresos</div>
		            <div class="infobox-content"><?= $resumen->row()->Total_egreso ?> Gs.</div>
		          </div>
		    </div>
		      
		    
		    <div class="infobox infobox-grey infobox-small infobox-dark col-xs-12 col-sm-4">
		          <div class="infobox-chart">
		            <span class="sparkline" data-values="3,4,2,3,4,4,2,2">
		              <i class="fa fa-money fa-2x" style="vertical-align: top"></i>
		            </span>
		          </div>

		          <div class="infobox-data">
		            <div class="infobox-content">Efectivo/rendir</div>
		            <div class="infobox-content"><?= $resumen->row()->Efectivo_a_rendir ?> Gs.</div>
		          </div>
		    </div>      
		</div>
	</div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-3" style="text-align: center">
  	<a href="<?= base_url('movimientos/ventas/ventas') ?>">
	    <div class="thumbnail">
	      <i class="fa fa-5x fa-shopping-cart"></i>
	      <div class="caption">
	        <h3>Ventas</h3>               
	      </div>
	    </div>
	</a>
  </div>

  <div class="col-sm-6 col-md-3" style="text-align: center">
  	<a href="<?= base_url('maestras/clientes') ?>">
	    <div class="thumbnail">
	      <i class="fa fa-5x fa-id-card"></i>
	      <div class="caption">
	        <h3>Clientes</h3>               
	      </div>
	    </div>
	</a>
  </div>

	<div class="col-sm-6 col-md-3" style="text-align: center">
	  	<a href="<?= base_url('movimientos/creditos/creditos') ?>">
		    <div class="thumbnail">
		      <i class="fa fa-5x fa-bank"></i>
		      <div class="caption">
		        <h3>Creditos</h3>               
		      </div>
		    </div>
		</a>
	  </div>

	  <div class="col-sm-6 col-md-3" style="text-align: center">
	  	<a href="<?= base_url('movimientos/productos/productos') ?>">
		    <div class="thumbnail">
		      <i class="fa fa-5x fa-cubes"></i>
		      <div class="caption">
		        <h3>Productos</h3>               
		      </div>
		    </div>
		</a>
	  </div>
</div>
<div class="row" style="margin:40px;">
    <div class="space-6"></div>

    <div class="col-sm-12 infobox-container">
        
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          
          <?php 
            $this->db->order_by('orden','ASC');
            foreach($this->db->get('tipo_contador')->result() as $t): 
          ?>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <?= $t->nombre ?>
                    </a>
                  </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <?php foreach($this->db->get_where('contadores',array('tipo_contador_id'=>$t->id))->result() as $c): ?>
                        <div class="infobox infobox-green">
                            <div class="infobox-icon">
                                <i class="ace-icon <?= $c->icono ?>"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">
                                    <?php 
                                        $query = $c->query;
                                        $query = str_replace('$_sucursales_id',$this->user->sucursal,$query);
                                        $query = str_replace('$_user_id',$this->user->id,$query);
                                        $query = str_replace('$_caja_id',$this->user->caja,$query);
                                        $query = str_replace('$_cajadiaria_id',$this->user->cajadiaria,$query);                                        
                                        echo $this->db->query(str_replace('|selec|','SELECT',$query))->row()->total 
                                    ?>
                                </span>
                                <div class="infobox-content"><?= $c->titulo ?></div>
                            </div>
                        </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
          

          


        



        
    </div> 
    
    <div class="vspace-12-sm"></div>

    <div class="col-sm-5">
        
    </div><!-- /.col -->
</div>


