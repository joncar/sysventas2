<?php

require_once APPPATH.'/controllers/Panel.php';    

class Maestras extends Panel {

    function __construct() {
        parent::__construct();             
    }

     public function tipo_contador($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->add_action('Detalles','',base_url('maestras/contadores/').'/');
        $output = $crud->render();            
        $this->loadView($output);
    }
    
    public function cantidades_mayoristas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->field_type('campo','dropdown',array('precio_venta_mayorista1'=>'Precio mayorista 1','precio_venta_mayorista2'=>'Precio mayorista 2','precio_venta_mayorista3'=>'Precio mayorista 3'));
        $output = $crud->render();            
        $this->loadView($output);
    }

    public function contadores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->where('tipo_contador_id',$x)
             ->field_type('tipo_contador_id',$x)
             ->unset_columns('tipo_contador_id');
        $output = $crud->render();        
        $this->loadView($output);
    }
    
    public function paises($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $output = $crud->render();
        $output->relatedlinks = array(array('admin/ciudades', 'Ciudades'));
        $this->loadView($output);
    }

    public function ciudades($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('pais', 'paises', 'denominacion');
        $output = $crud->render();
        $output->relatedlinks = array(array('admin/ciudades', 'Ciudades'));
        $this->loadView($output);
    }

    public function barrios($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('pais', 'paises', 'denominacion');
        $crud->set_relation('ciudad', 'ciudades', 'denominacion');
        $output = $crud->render();        
        $this->loadView($output);
    }
    
    public function proveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('ciudad', 'ciudades', 'denominacion');
        $crud->set_relation('pais_id', 'paises', 'denominacion');
        $crud->set_relation('tipo_proveedor', 'tipo_proveedores', 'denominacion');
        $crud->required_fields('denominacion', 'ruc', 'direccion', 'ciudad', 'pais_id', 'telefonofijo', 'telefax', 'celular', 'email');
        $crud->unset_fields('created', 'modified');
        $crud->field_type('usuario_id', 'hidden', $_SESSION['user']);
        $crud->columns('denominacion', 'tipo_proveedor', 'ruc', 'telefonofijo', 'celular', 'Observacion');
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function sucursales($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $crud->set_relation('pais_id', 'paises', 'denominacion');
        $crud->set_relation('ciudad_id', 'ciudades', 'denominacion');
        $crud->set_relation_dependency('ciudad_id', 'pais_id', 'pais');
        $crud->field_type('principal', 'true_false', array('0' => 'No', '1' => 'Si'));

        $crud->callback_before_insert(function($post) {
            if ($post['principal'] == 1)
                $this->db->update('sucursales', array('principal' => 0));
        });
        $crud->callback_before_update(function($post) {
            if ($post['principal'] == 1)
                $this->db->update('sucursales', array('principal' => 0));
        });
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function getUltimoCliente(){
        if(!empty($_SESSION['refreshClientes'])){ //variable que viene desde añadir clientes
            $this->db->order_by('id','DESC');
            $clientes = $this->db->get_where('clientes',array('inactivo'=>0));
            $cliente = array();
            if($clientes->num_rows()>0){
                $cliente = $clientes->row();
            }
            $cliente->refresh = true;
            echo json_encode($cliente);
        }
    }
    
    public function clientes($x = '', $y = '', $return = false) {
        $crud = parent::crud_function($x, $y);        
        $crud->field_type('tipo_doc', 'hidden','')            
            ->set_relation('pais','paises','denominacion')
            ->set_relation('ciudad','ciudades','denominacion')
            ->set_relation('barrio','barrios','denominacion')                      
            ->field_type('celular2', 'hidden','')
            ->field_type('celular3', 'hidden','')
            ->field_type('email', 'hidden','')            
            ->field_type('foto','image',array('path'=>'img/clientes','width'=>'50px','height'=>'50px'));        
        $crud->columns('id','nombres','apellidos','nro_documento','celular','barrio','ciudad','mayorista');
        $crud->required_fields('nombres','apellidos');
        $crud->set_rules('email', 'Email', 'valid_email');
        
        if ($x == 'add' || $x == 'insert' || $x == 'insert_validation') {
            $crud->set_rules('nro_documento', 'Cedula', 'required|is_unique[clientes.nro_documento]');
        }
        $crud->unset_fields('created', 'modified');
        if (!empty($x) && !empty($y) && $y == 'json') {
            $crud->unset_back_to_list();            
        }
        if($x=='json_list'){
            $crud->limit(10000);
        }        
        $output = $crud->render();
        $output->crud = 'clientes';
        if (!empty($x) && !empty($y) && $y == 'json') {
            $output->json = 'Activo';
            $output->view = 'json';
        }
        if($crud->getParameters(TRUE)!='update' || $crud->getParameters(TRUE)!='insert'){
            $crud->set_relation_dependency('ciudad','pais','pais')            
                     ->set_relation_dependency('barrio','ciudad','ciudad');
        }
        if($crud->getParameters(FALSE)=='add' || $crud->getParameters(FALSE)=='edit'){            
            $edit = (is_numeric($y))?$edit = $this->db->get_where('clientes',array('id'=>$y)):'';
            $output->output = $this->load->view('clientes',array('o'=>$output->output,'edit'=>$edit),TRUE);
        }
        $this->loadView($output);
    }
    
    public function monedas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->field_type('primario', 'true_false', array('0' => 'No', '1' => 'Si'));
        $crud->unset_delete();
        $output = $crud->render();
        $output->crud = 'user';
        $this->loadView($output);
    }
    
    public function motivo_salida($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function motivo_entrada($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function tipo_proveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }
    
    public function cuentas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);        
        $output = $crud->render();
        $this->loadView($output);
    }  
    
    function condiciones(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Condiciones de pago');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Condiciones de pago';
            $this->loadView($crud);
        }
        function estados(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Estados de ocupacion');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Estados de ocupacion';
            $this->loadView($crud);
        }     

    public function unidad_medida($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);    
        $crud->set_subject('Unidades de medida');
        $crud->columns('id','denominacion');
        if($crud->getParameters()=='add'){
            $crud->set_rules('denominacion','Denominación','required|is_unique[unidad_medida.denominacion]');
        }
        $output = $crud->render();            
        $output->title = 'Unidades de medida';
        $this->loadView($output);
    }   
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
