<?php if(empty($_POST) || empty($_POST['anio'])): ?>
<div class="container">
    <h1 align="center"> Resumen Anual de ventas</h1>
<form action="<?= base_url('reportes/resumen_anual') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputPassword1">Año</label>
    <input type="number" name="anio" class="form-control" id="desde">
  </div>    
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>    
    <h1 align="center"> Resumen Anual <?= $_POST['anio'] ?></h1>    
    <?php 
            $sucursales = $this->db->get_where('sucursales',array('id != '=>5));             
    ?>
    <?php 
        $totalventaanual = 0; 
        $totalcontadoanual = 0; 
        $totalcreditoanual = 0; 
        $totaldescuentoanual = 0; 
     ?>
    <?php foreach($sucursales->result() as $s): ?>
        <?php 
            $total = 0;
            $total_venta = 0;
            $total_contado = 0;
            $total_credito = 0;
            $total_descuento = 0;
            $data = $this->db->query("
                SELECT 
                        'Ene',
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 1 AND YEAR(fecha)=".$_POST['anio']." AND sucursal=".$s->id.") AS total_venta,
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 1 AND YEAR(fecha)=".$_POST['anio']." AND transaccion=1 AND sucursal=".$s->id.") AS total_contado, 
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 1 AND YEAR(fecha)=".$_POST['anio']." AND transaccion =2
                        AND sucursal=".$s->id.") AS total_credito,

                        (SELECT SUM(total_descuentos) FROM ventas WHERE MONTH(fecha) = 1 AND YEAR(fecha)=".$_POST['anio']."
                        AND sucursal=".$s->id.") AS total_descuento

                        UNION SELECT 
                        'Feb',
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 2 AND YEAR(fecha)=".$_POST['anio']." AND sucursal=".$s->id.") AS total_venta,
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 2 AND YEAR(fecha)=".$_POST['anio']." AND transaccion=1 AND sucursal=".$s->id.") AS total_contado, 
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 2 AND YEAR(fecha)=".$_POST['anio']." AND transaccion =2
                        AND sucursal=".$s->id.") AS total_credito,

                        (SELECT SUM(total_descuentos) FROM ventas WHERE MONTH(fecha) = 2 AND YEAR(fecha)=".$_POST['anio']."
                        AND sucursal=".$s->id.") AS total_descuento

                        UNION SELECT 
                        'Mar',
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 3 AND YEAR(fecha)=".$_POST['anio']." AND sucursal=".$s->id.") AS total_venta,
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 3 AND YEAR(fecha)=".$_POST['anio']." AND transaccion=1 AND sucursal=".$s->id.") AS total_contado, 
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 3 AND YEAR(fecha)=".$_POST['anio']." AND transaccion =2
                        AND sucursal=".$s->id.") AS total_credito,

                        (SELECT SUM(total_descuentos) FROM ventas WHERE MONTH(fecha) = 3 AND YEAR(fecha)=".$_POST['anio']."
                        AND sucursal=".$s->id.") AS total_descuento

                        UNION SELECT 
                        'Abr',
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 4 AND YEAR(fecha)=".$_POST['anio']." AND sucursal=".$s->id.") AS total_venta,
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 4 AND YEAR(fecha)=".$_POST['anio']." AND transaccion=1 AND sucursal=".$s->id.") AS total_contado, 
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 4 AND YEAR(fecha)=".$_POST['anio']." AND transaccion =2
                        AND sucursal=".$s->id.") AS total_credito,

                        (SELECT SUM(total_descuentos) FROM ventas WHERE MONTH(fecha) = 4 AND YEAR(fecha)=".$_POST['anio']."
                        AND sucursal=".$s->id.") AS total_descuento

                        UNION SELECT 
                        'May',
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 5 AND YEAR(fecha)=".$_POST['anio']." AND sucursal=".$s->id.") AS total_venta,
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 5 AND YEAR(fecha)=".$_POST['anio']." AND transaccion=1 AND sucursal=".$s->id.") AS total_contado, 
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 5 AND YEAR(fecha)=".$_POST['anio']." AND transaccion =2
                        AND sucursal=".$s->id.") AS total_credito,

                        (SELECT SUM(total_descuentos) FROM ventas WHERE MONTH(fecha) = 5 AND YEAR(fecha)=".$_POST['anio']."
                        AND sucursal=".$s->id.") AS total_descuento

                        UNION SELECT 
                        'Jun',
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 6 AND YEAR(fecha)=".$_POST['anio']." AND sucursal=".$s->id.") AS total_venta,
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 6 AND YEAR(fecha)=".$_POST['anio']." AND transaccion=1 AND sucursal=".$s->id.") AS total_contado, 
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 6 AND YEAR(fecha)=".$_POST['anio']." AND transaccion =2
                        AND sucursal=".$s->id.") AS total_credito,

                        (SELECT SUM(total_descuentos) FROM ventas WHERE MONTH(fecha) = 6 AND YEAR(fecha)=".$_POST['anio']."
                        AND sucursal=".$s->id.") AS total_descuento

                        UNION SELECT 
                        'Jul',
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 7 AND YEAR(fecha)=".$_POST['anio']." AND sucursal=".$s->id.") AS total_venta,
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 7 AND YEAR(fecha)=".$_POST['anio']." AND transaccion=1 AND sucursal=".$s->id.") AS total_contado, 
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 7 AND YEAR(fecha)=".$_POST['anio']." AND transaccion =2
                        AND sucursal=".$s->id.") AS total_credito,

                        (SELECT SUM(total_descuentos) FROM ventas WHERE MONTH(fecha) = 7 AND YEAR(fecha)=".$_POST['anio']."
                        AND sucursal=".$s->id.") AS total_descuento

                        UNION SELECT 
                        'Ago',
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 8 AND YEAR(fecha)=".$_POST['anio']." AND sucursal=".$s->id.") AS total_venta,
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 8 AND YEAR(fecha)=".$_POST['anio']." AND transaccion=1 AND sucursal=".$s->id.") AS total_contado, 
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 8 AND YEAR(fecha)=".$_POST['anio']." AND transaccion =2
                        AND sucursal=".$s->id.") AS total_credito,

                        (SELECT SUM(total_descuentos) FROM ventas WHERE MONTH(fecha) = 8 AND YEAR(fecha)=".$_POST['anio']."
                        AND sucursal=".$s->id.") AS total_descuento

                        UNION SELECT 
                        'Sep',
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 9 AND YEAR(fecha)=".$_POST['anio']." AND sucursal=".$s->id.") AS total_venta,
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 9 AND YEAR(fecha)=".$_POST['anio']." AND transaccion=1 AND sucursal=".$s->id.") AS total_contado, 
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 9 AND YEAR(fecha)=".$_POST['anio']." AND transaccion =2
                        AND sucursal=".$s->id.") AS total_credito,

                        (SELECT SUM(total_descuentos) FROM ventas WHERE MONTH(fecha) = 9 AND YEAR(fecha)=".$_POST['anio']."
                        AND sucursal=".$s->id.") AS total_descuento

                        UNION SELECT 
                        'Oct',
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 10 AND YEAR(fecha)=".$_POST['anio']." AND sucursal=".$s->id.") AS total_venta,
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 10 AND YEAR(fecha)=".$_POST['anio']." AND transaccion=1 AND sucursal=".$s->id.") AS total_contado, 
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 10 AND YEAR(fecha)=".$_POST['anio']." AND transaccion =2
                        AND sucursal=".$s->id.") AS total_credito,

                        (SELECT SUM(total_descuentos) FROM ventas WHERE MONTH(fecha) = 10 AND YEAR(fecha)=".$_POST['anio']."
                        AND sucursal=".$s->id.") AS total_descuento

                        UNION SELECT 
                        'Nov',
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 11 AND YEAR(fecha)=".$_POST['anio']." AND sucursal=".$s->id.") AS total_venta,
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 11 AND YEAR(fecha)=".$_POST['anio']." AND transaccion=1 AND sucursal=".$s->id.") AS total_contado, 
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 11 AND YEAR(fecha)=".$_POST['anio']." AND transaccion =2
                        AND sucursal=".$s->id.") AS total_credito,

                        (SELECT SUM(total_descuentos) FROM ventas WHERE MONTH(fecha) = 11 AND YEAR(fecha)=".$_POST['anio']."
                        AND sucursal=".$s->id.") AS total_descuento

                        UNION SELECT 
                        'Dic',
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 12 AND YEAR(fecha)=".$_POST['anio']." AND sucursal=".$s->id.") AS total_venta,
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 12 AND YEAR(fecha)=".$_POST['anio']." AND transaccion=1 AND sucursal=".$s->id.") AS total_contado, 
                        (SELECT SUM(total_venta) FROM ventas WHERE MONTH(fecha) = 12 AND YEAR(fecha)=".$_POST['anio']." AND transaccion =2
                        AND sucursal=".$s->id.") AS total_credito,

                        (SELECT SUM(total_descuentos) FROM ventas WHERE MONTH(fecha) = 12 AND YEAR(fecha)=".$_POST['anio']."
                        AND sucursal=".$s->id.") AS total_descuento
                    ");
        ?>
        <p><b><?= $s->denominacion ?></b></p>
        <table class="table" width="100%" style="font-size:12px;">
            <thead>
                    <tr>
                            <th style="width:100px">Mes</th>
                            <th style="width:200px">Total Venta</th>
                            <th style="width:200px">Total Contado</th>
                            <th style="width:200px">Total Crédito</th>
                            <th style="width:200px">Total Descuento</th>
                    </tr>
            </thead>
            <tbody>    
                <?php $total = 0; ?>
                <?php foreach($data->result() as $c): ?>
                    <tr>                        
                            <td><?= $c->Ene ?></td>
                            <td><?= number_format($c->total_venta,2,',','.') ?></td>
                            <td><?= number_format($c->total_contado,2,',','.') ?></td>
                            <td><?= number_format($c->total_credito,2,',','.') ?></td>
                            <td><?= number_format($c->total_descuento,2,',','.') ?></td>                            
                    </tr>
                    <?php 
                        $total_venta+= $c->total_venta;
                        $total_contado+= $c->total_contado;
                        $total_credito+= $c->total_credito;
                        $total_descuento+= $c->total_descuento;                        
                    ?>
                <?php endforeach ?>
                    <tr>                        
                            <td><b>Total</b></td>                            
                            <td><b><?= number_format($total_venta,2,',','.') ?></b></td>
                            <td><b><?= number_format($total_contado,2,',','.') ?></b></td>
                            <td><b><?= number_format($total_credito,2,',','.') ?></b></td>
                            <td><b><?= number_format($total_descuento,2,',','.') ?></b></td>
                    </tr>
                    <?php 
                        $totalventaanual+= $total_venta; 
                        $totalcontadoanual+= $total_contado; 
                        $totalcreditoanual+= $total_credito; 
                        $totaldescuentoanual+= $total_descuento; 
                    ?>
            </tbody>
        </table>
    <?php endforeach ?>
        <p><b>Total Venta: </b><?= number_format($totalventaanual,2,',','.') ?></p>
        <p><b>Total Contado: </b><?= number_format($totalcontadoanual,2,',','.') ?></p>
        <p><b>Total Crédito: </b><?= number_format($totalcreditoanual,2,',','.') ?></p>
        <p><b>Total Descuento: </b><?= number_format($totaldescuentoanual,2,',','.') ?></p>
<?php endif; ?>