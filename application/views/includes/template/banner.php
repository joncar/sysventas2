<section id="banner" style="padding:0px;">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">       
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            
            <?php 
                $this->db->order_by('orden','ASC');
                foreach($this->db->get_where('banner',array('activo'=>1))->result() as $n=>$b): 
            ?>
            <div class="item <?= $n==0?'active':'' ?>" style="background:url('<?= base_url('img/banner/'.$b->foto) ?>') no-repeat; background-size:cover; height:600px; width:100%;">
            <div class="carousel-caption" style="width:500px; <?= $b->posicion ?>">
              <div>
                    <?= $b->texto ?>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
            
        </div>
      </div>
</section>
<script>
    $('.carousel').carousel({
        interval: 1000 *  <?= $this->db->get('ajustes')->row()->banner_tiempo_transicion ?>
    });
</script>