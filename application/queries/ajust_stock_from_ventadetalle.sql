#función para actualizar stock desde after_insert_ventadetalle ID de ventadetalle
#PARAMS VentaDetalle INT
BEGIN
DECLARE productoV varchar(255);
DECLARE cantidadV INT;
DECLARE i INT;
DECLARE control_stock INT;
SET i = 0;

SELECT vender_sin_stock INTO control_stock FROM ajustes limit 1;

IF(control_stock=0) THEN
SELECT 
COUNT(producto) INTO cantidadV
FROM (
SELECT
ventadetalle.producto
FROM ventadetalle 
INNER JOIN ventas ON ventas.id = ventadetalle.venta
INNER JOIN productos ON productos.codigo = ventadetalle.producto
WHERE ventadetalle.id = VentaDetalle AND 
(ventas.status IS NULL OR ventas.status = 0)
UNION ALL
SELECT 
descontarde.codigo
FROM ventadetalle
INNER JOIN ventas ON ventas.id = ventadetalle.venta
INNER JOIN productos ON productos.codigo = ventadetalle.producto
INNER JOIN producto_asociado ON producto_asociado.productos_id = productos.id
INNER JOIN productos as descontarde ON descontarde.id = producto_asociado.descontar
WHERE ventadetalle.id = VentaDetalle AND (ventas.status IS NULL OR ventas.status = 0)) AS total_venta;

WHILE i < cantidadV DO

SELECT 
producto INTO productoV
FROM (
SELECT
ventadetalle.producto
FROM ventadetalle 
INNER JOIN ventas ON ventas.id = ventadetalle.venta
INNER JOIN productos ON productos.codigo = ventadetalle.producto
WHERE ventadetalle.id = VentaDetalle AND 
(ventas.status IS NULL OR ventas.status = 0)
UNION ALL
SELECT 
descontarde.codigo
FROM ventadetalle
INNER JOIN ventas ON ventas.id = ventadetalle.venta
INNER JOIN productos ON productos.codigo = ventadetalle.producto
INNER JOIN producto_asociado ON producto_asociado.productos_id = productos.id
INNER JOIN productos as descontarde ON descontarde.id = producto_asociado.descontar
WHERE ventadetalle.id = VentaDetalle AND (ventas.status IS NULL OR ventas.status = 0)) AS total_venta LIMIT i,1;

CALL set_stock(productoV);
set i = i+1;
END WHILE;
END IF;
END